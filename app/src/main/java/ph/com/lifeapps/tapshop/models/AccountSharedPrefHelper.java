package ph.com.lifeapps.tapshop.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import ph.com.lifeapps.tapshop.activity.BaseActivity;

/**
 * Created by Lem on 7/12/2016.
 */
public class AccountSharedPrefHelper {

    private static String PREF_NAME = "account_pref";
    private static String ACCOUNT = "account";

    private static AccountSharedPrefHelper accountSharedPrefHelper;
    private SharedPreferences sharedPreferences;

    private AccountSharedPrefHelper(BaseActivity baseActivity){
        sharedPreferences = baseActivity.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static AccountSharedPrefHelper getInstance(BaseActivity baseActivity){
        if (accountSharedPrefHelper ==null){
            accountSharedPrefHelper = new AccountSharedPrefHelper(baseActivity);
        }
        return accountSharedPrefHelper;
    }

    public void update(String accountJSON){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(ACCOUNT, accountJSON);
        editor.commit();
    }

    public void clear(){
        sharedPreferences.edit().clear().commit();
    }

    public void update(Account account){
        update(new Gson().toJson(account));
    }

    public Account getACCOUNT(){
        String account = sharedPreferences.getString(ACCOUNT, "");
        return account.length() == 0 ? null : new Gson().fromJson(account, Account.class);
    }
}
