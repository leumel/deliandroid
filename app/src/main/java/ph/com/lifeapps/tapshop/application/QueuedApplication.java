package ph.com.lifeapps.tapshop.application;

import android.app.Application;

import ph.com.lifeapps.tapshop.R;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by Lem on 7/4/2016.
 */
public class QueuedApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("Aileron-Regular.otf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
    }
}
