package ph.com.lifeapps.tapshop.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.iid.FirebaseInstanceId;
import com.kbeanie.multipicker.api.ImagePicker;
import com.kbeanie.multipicker.api.Picker;
import com.kbeanie.multipicker.api.callbacks.ImagePickerCallback;
import com.kbeanie.multipicker.api.entity.ChosenFile;
import com.kbeanie.multipicker.api.entity.ChosenImage;

import java.io.File;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import ph.com.lifeapps.tapshop.R;
import ph.com.lifeapps.tapshop.models.Account;
import ph.com.lifeapps.tapshop.models.AccountHolder;
import ph.com.lifeapps.tapshop.models.AccountSharedPrefHelper;
import ph.com.lifeapps.tapshop.models.ObjectHolder;
import ph.com.lifeapps.tapshop.models.Products;
import ph.com.lifeapps.tapshop.network.TapClient;
import ph.com.lifeapps.tapshop.utils.CircleTransform;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Field;

/**
 * Created by Lem on 7/8/2016.
 */
public class SignupActivity extends BaseActivity implements ImagePickerCallback {

    @BindView(R.id.et_name)                         EditText etName;
    @BindView(R.id.et_email_add)                    EditText etEmail;
    @BindView(R.id.et_password)                     EditText etPword;
    @BindView(R.id.et_contact_num)                  EditText etContactNum;
    @BindView(R.id.iv_add_photo)                    ImageView ivPhoto;

    private ProgressDialog dialog;
    private Account account;
    private ImagePicker imagePicker;

    private File file;

    @Override
    protected int getLayout() {
        return R.layout.activity_signup;
    }

    @Override
    protected void initViews() {

    }

    @OnClick(R.id.iv_add_photo)
    public void addPhoto(){
        imagePicker = new ImagePicker(this);
        imagePicker.setImagePickerCallback(this);
        imagePicker.shouldGenerateThumbnails(false);
        imagePicker.shouldGenerateMetadata(false);
        imagePicker.pickImage();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            if(requestCode == Picker.PICK_IMAGE_DEVICE) {
                if(imagePicker == null) {
                    imagePicker = new ImagePicker(this);
                    imagePicker.setImagePickerCallback(this);
                }
                imagePicker.submit(data);
            }
        }
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
    }

    @OnClick(R.id.btn_register)
    public void register(){
        RequestBody fbody = RequestBody.create(MediaType.parse("image"), file);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), getString(etName));
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), getString(etEmail));
        RequestBody pword = RequestBody.create(MediaType.parse("text/plain"), getString(etPword));
        RequestBody contactNo = RequestBody.create(MediaType.parse("text/plain"), getString(etContactNum));


        dialog = ProgressDialog.show(this, "Loading...", "Signing you up!", false);
        TapClient.getInstance().getService().register(name, email, pword, contactNo, fbody).enqueue(new Callback<ObjectHolder<AccountHolder>>() {
            @Override
            public void onResponse(Call<ObjectHolder<AccountHolder>> call, Response<ObjectHolder<AccountHolder>> response) {
                dialog.dismiss();
                if (response.isSuccessful() && response.body().isSuccess()){
                    account = response.body().getData().getCustomer();
                    login();
                }else {
                    Toast.makeText(SignupActivity.this, "Failed to sign you up", Toast.LENGTH_SHORT);
                }
            }

            @Override
            public void onFailure(Call<ObjectHolder<AccountHolder>> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(SignupActivity.this, "Failed to sign you up", Toast.LENGTH_SHORT);
            }
        });
    }

    private void login(){
        dialog = ProgressDialog.show(SignupActivity.this, "Loading...", "Logging you in!", false);
        TapClient.getInstance().getService().login(getString(etEmail), getString(etPword), FirebaseInstanceId.getInstance().getToken()).enqueue(new Callback<ObjectHolder<Products>>() {
            @Override
            public void onResponse(Call<ObjectHolder<Products>> call, Response<ObjectHolder<Products>> response) {
                if (response.isSuccessful()){
                    AccountSharedPrefHelper.getInstance(SignupActivity.this).update(account);
                    startActivity(new Intent(SignupActivity.this, MainActivity.class));
                }else {
                    Toast.makeText(SignupActivity.this, "Failed to Log you in", Toast.LENGTH_SHORT);
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ObjectHolder<Products>> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(SignupActivity.this, "Failed to Log you in", Toast.LENGTH_SHORT);
            }
        });
    }

    private String getString(EditText text){
        return text.getText().toString();
    }

    @Override
    public void onImagesChosen(List<ChosenImage> list) {
        if (list!=null && list.size()!=0){
            ChosenImage chosenImage = list.get(0);
            file = new File(chosenImage.getOriginalPath());
            if (file==null){
                Log.d("lem", "shit its null");
            }else{
                Log.d("lem", "shit yeah its not null");
            }
            Log.d("lem", "chosenImage.getOriginalPath() " + chosenImage.getOriginalPath());

            Glide.with(this).load(chosenImage.getOriginalPath()).transform(new CircleTransform(this)).into(ivPhoto);
        }
    }

    @Override
    public void onError(String s) {

    }
}


