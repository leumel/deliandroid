package ph.com.lifeapps.tapshop.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.lifeapps.tapshop.R;
import ph.com.lifeapps.tapshop.interfaces.OnItemClick;
import ph.com.lifeapps.tapshop.models.Product;
import ph.com.lifeapps.tapshop.utils.StringUtils;

/**
 * Created by Lem on 6/25/2016.
 */
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ProductViewHolder> {

    private final String IMAGE_ENDPOINT = "http://ec2-54-254-151-183.ap-southeast-1.compute.amazonaws.com/images/products/";

    private List<Product> data;

    public HashMap<Product, Integer> getCart() {
        return cart;
    }

    private HashMap<Product, Integer> cart;

    private Context context;
    private OnItemClick onItemClick;

    private OnCartUpdateListener onCartUpdateListener;

    public interface OnCartUpdateListener{
        void onChanged(int size);
    }

    public ProductAdapter(List<Product> item, OnItemClick onItemClick, OnCartUpdateListener onCartUpdateListener) {
        this.data = item;
        this.onItemClick = onItemClick;
        this.cart = new HashMap<>();
        this.onCartUpdateListener = onCartUpdateListener;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (context == null) {
            context = parent.getContext();
        }
        return new ProductViewHolder(LayoutInflater.from(context).inflate(R.layout.item_product, parent, false));
    }

    @Override
    public void onBindViewHolder(final ProductViewHolder holder, int position) {
        final Product product = data.get(position);
        holder.tvItemName.setText(product.getName());
        holder.tvItemPriceInfo.setText(StringUtils.getPriceInfo(product));
        Glide.with(context).load(IMAGE_ENDPOINT + product.getImage_url()).animate(android.R.anim.fade_in).into(holder.ivImageProduct);
        holder.btnAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.llCartController.getVisibility() == View.GONE) {
                    holder.llCartController.setVisibility(View.VISIBLE);
                    holder.btnAddToCart.setVisibility(View.GONE);
                }
            }
        });
        holder.btnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cart.put(product, cart.get(product) == null ? 0 : cart.get(product) - 1);
                if (cart.get(product) == null || cart.get(product) == 0) {
                    cart.remove(product);
                    if (onCartUpdateListener!=null){
                        onCartUpdateListener.onChanged(0);
                    }
                    holder.llCartController.setVisibility(View.GONE);
                    holder.btnAddToCart.setVisibility(View.VISIBLE);
                    holder.tvCartCount.setText(Integer.toString(0));
                    return;
                }
                holder.tvCartCount.setText(Integer.toString(cart.get(product)));
            }
        });
        holder.btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cart.get(product) == null) {
                    cart.put(product, 1);
                    if (onCartUpdateListener!=null){
                        onCartUpdateListener.onChanged(cart.size());
                    }
                } else {
                    cart.put(product, cart.get(product) == null ? 1 : cart.get(product) + 1);
                }
                holder.tvCartCount.setText(Integer.toString(cart.get(product)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ProductViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tv_item_name)
        TextView tvItemName;
        @BindView(R.id.tv_item_price_info)
        TextView tvItemPriceInfo;
        @BindView(R.id.iv_image_preview)
        ImageView ivImageProduct;
        @BindView(R.id.btn_add_to_cart)
        Button btnAddToCart;
        @BindView(R.id.ll_cart_controller)
        View llCartController;
        @BindView(R.id.btn_minus)
        Button btnMinus;
        @BindView(R.id.btn_add)
        Button btnAdd;
        @BindView(R.id.tv_cart_count)
        TextView tvCartCount;

        public ProductViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
