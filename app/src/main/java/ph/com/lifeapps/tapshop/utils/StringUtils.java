package ph.com.lifeapps.tapshop.utils;

import java.text.DecimalFormat;

import ph.com.lifeapps.tapshop.models.Product;

/**
 * Created by Lem on 7/7/2016.
 */
public class StringUtils {
    public static String getPriceInfo(Product product){
        DecimalFormat formatter = new DecimalFormat("#,###.##");
        return "P "+formatter.format(product.getPrice())+"/"+product.getMeasurement();
    }

    public static String pricify(Product product){
        DecimalFormat formatter = new DecimalFormat("#,###.##");
        return "P "+formatter.format(product.getPrice()*product.getCount());
    }

    public static String pricify(double price){
        DecimalFormat formatter = new DecimalFormat("#,###.##");
        return "P "+formatter.format(price);
    }
}
