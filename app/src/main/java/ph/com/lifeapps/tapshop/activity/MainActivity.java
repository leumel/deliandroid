package ph.com.lifeapps.tapshop.activity;

import android.Manifest;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.OnClick;
import ph.com.lifeapps.tapshop.R;
import ph.com.lifeapps.tapshop.adapters.ViewPagerAdapter;
import ph.com.lifeapps.tapshop.fragment.ComingSoonFragment;
import ph.com.lifeapps.tapshop.fragment.MapFragment;
import ph.com.lifeapps.tapshop.fragment.ProfileFragment;
import ph.com.lifeapps.tapshop.utils.AndroidPermissions;
import ph.com.lifeapps.tapshop.utils.TapUtils;

/**
 * Created by Lem on 6/15/2016.
 */
public class MainActivity extends BaseActivity {

    @BindView(R.id.iv_back)                         ImageView ivBackButton;
    @BindView(R.id.vp_fragment_holder)              ViewPager vpFragmentHolder;
    @BindView(R.id.tabs)                            TabLayout tabs;

    private ViewPagerAdapter viewPagerAdapter;
    private AndroidPermissions mPermissions;

    @Override
    protected int getLayout() {
        return R.layout.activity_map;
    }

    @Override
    protected void initViews() {
        Log.d("lem", "reg id "+ TapUtils.getDeviceToken());
        ivBackButton.setVisibility(View.GONE);
        setupViewPager(vpFragmentHolder);
        tabs.setupWithViewPager(vpFragmentHolder);
        setupTabIcons();
        mPermissions = new AndroidPermissions(this,
                Manifest.permission.ACCESS_COARSE_LOCATION,
                Manifest.permission.ACCESS_FINE_LOCATION);
        requestMissingPermissions();

    }


    private void setupTabIcons() {
        tabs.getTabAt(0).setIcon(R.drawable.ic_list_deactivated);
        tabs.getTabAt(1).setIcon(R.drawable.ic_search_deactivated);
        tabs.getTabAt(2).setIcon(R.drawable.ic_map_deactivated);
        tabs.getTabAt(3).setIcon(R.drawable.ic_order_deactivated);
        tabs.getTabAt(4).setIcon(R.drawable.ic_user_deactivated);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ComingSoonFragment());
        adapter.addFragment(new ComingSoonFragment());
        adapter.addFragment(new MapFragment());
        adapter.addFragment(new ComingSoonFragment());
        adapter.addFragment(new ProfileFragment());
        viewPager.setAdapter(adapter);
    }

    @OnClick(R.id.iv_notif_icon)
    public final void showNotifications(){
        startActivity(new Intent(this, NotificationActivity.class));
    }

    private void requestMissingPermissions() {
        if (mPermissions.checkPermissions()) {
            vpFragmentHolder.setCurrentItem(2);
        } else {
            mPermissions.requestPermissions(101);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (mPermissions.areAllRequiredPermissionsGranted(grantResults)) {
            vpFragmentHolder.setCurrentItem(2);
        } else {
            onInsufficientPermissions();
        }
    }

    private void onInsufficientPermissions() {
        //your implementation to show the user that the required permissions have not been granted
    }
}
