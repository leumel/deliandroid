package ph.com.lifeapps.tapshop.activity;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnClick;
import ph.com.lifeapps.tapshop.R;
import ph.com.lifeapps.tapshop.adapters.ProductAdapter;
import ph.com.lifeapps.tapshop.interfaces.OnItemClick;
import ph.com.lifeapps.tapshop.models.Branch;
import ph.com.lifeapps.tapshop.models.ObjectHolder;
import ph.com.lifeapps.tapshop.models.Products;
import ph.com.lifeapps.tapshop.network.TapClient;
import ph.com.lifeapps.tapshop.utils.TapUtils;
import ph.com.lifeapps.tapshop.widgets.GridAutofitLayoutManager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by lemuel.castro on 6/21/2016.
 */
public class ShopActivity extends BaseActivity implements OnItemClick, ProductAdapter.OnCartUpdateListener {

    public static final String SHOP = "SHOP_EXTRA";
    private static final int PRODUCT_VIEW_WIDTH = 182;

    private Branch branch;

    @BindView(R.id.toolbar_shop)        Toolbar toolbar;
    @BindView(R.id.rv_products)         RecyclerView rvProducts;
    @BindView(R.id.tv_branch_name)      TextView tvBranchName;
    @BindView(R.id.tv_cart_item_count)  TextView tvCartItemCount;
    @BindView(R.id.ic_product_state_indicatior) ImageView ivProductStateIndicator;

    private ProductAdapter productAdapter;

    @Override
    protected int getLayout() {
        return R.layout.activity_shop;
    }

    @Override
    protected void initViews() {
        branch = new Gson().fromJson(getIntent().getStringExtra(SHOP), Branch.class);
        tvBranchName.setText(branch.getName());
        setSupportActionBar(toolbar);
        GridAutofitLayoutManager gridAutofitLayoutManager = new GridAutofitLayoutManager(this, 1);
        gridAutofitLayoutManager.setColumnWidth(TapUtils.dpToPx(this, PRODUCT_VIEW_WIDTH));
        rvProducts.setLayoutManager(gridAutofitLayoutManager);
        getProducts("1");
    }

    @OnClick(R.id.iv_shop)
    public final void showCart() {
        Intent intent = new Intent(this, CartActivity.class);
        intent.putExtra(CartActivity.CART_EXTRA, productAdapter.getCart());
        intent.putExtra(CartActivity.BRANCH_EXTRA, new Gson().toJson(branch));
        startActivity(intent);
    }

    private void getProducts(String tagID) {
        TapClient.getInstance().getService().getProductByTag(Integer.toString(branch.getId()), tagID,TapUtils.getDeviceToken()).enqueue(new Callback<ObjectHolder<Products>>() {
            @Override
            public void onResponse(Call<ObjectHolder<Products>> call, Response<ObjectHolder<Products>> response) {
                productAdapter = new ProductAdapter(response.body().getData().getProducts(), ShopActivity.this, ShopActivity.this);
                rvProducts.setAdapter(productAdapter);
                if (response.body().getData().getProducts().size() == 0){
                    fade(ivProductStateIndicator, rvProducts);
                    Glide.with(ShopActivity.this).load(R.drawable.ic_no_products).into(ivProductStateIndicator);
                }else {
                    fade(rvProducts, ivProductStateIndicator);
                }
            }

            @Override
            public void onFailure(Call<ObjectHolder<Products>> call, Throwable t) {

            }
        });
    }

    private void fade(final View toFadeIn, final View toFadeOut){
        ObjectAnimator fadein = ObjectAnimator.ofFloat(toFadeIn, "Alpha", 0.0f, 1.0f);
        ObjectAnimator fadeout = ObjectAnimator.ofFloat(toFadeOut, "Alpha", 1.0f, 0.0f);

        fadeout.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                toFadeIn.setVisibility(View.VISIBLE);
                toFadeOut.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });

        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.playSequentially(fadeout, fadein);
        animatorSet.setDuration(300);
        animatorSet.start();
        toFadeOut.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(int position, Object object) {

    }

    @Override
    public void onChanged(int size) {
        tvCartItemCount.setText(Integer.toString(size));
    }
}
