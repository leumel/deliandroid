package ph.com.lifeapps.tapshop.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.OnClick;
import ph.com.lifeapps.tapshop.R;
import ph.com.lifeapps.tapshop.models.AccountSharedPrefHelper;
import ph.com.lifeapps.tapshop.models.ObjectHolder;
import ph.com.lifeapps.tapshop.models.Products;
import ph.com.lifeapps.tapshop.network.TapClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.et_email)                EditText etEmail;
    @BindView(R.id.et_pword)                EditText etPword;

    private ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("lem", "token "+FirebaseInstanceId.getInstance().getToken());
        if (AccountSharedPrefHelper.getInstance(this).getACCOUNT() != null) {
            startActivity(new Intent(this, MainActivity.class));
        }
    }

    @Override
    protected int getLayout() {
        return R.layout.activity_login;
    }

    @Override
    protected void initViews() {

    }

    @OnClick(R.id.btn_login)
    public final void openMainActivity() {
        startActivity(new Intent(this, MainActivity.class));
    }

    @OnClick(R.id.btn_signup)
    public final void openSignupActivity() {
        startActivity(new Intent(this, SignupActivity.class));
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    private void login(){
        dialog = ProgressDialog.show(this, "Loading...", "Logging you in!", false);
        TapClient.getInstance().getService().login(getString(etEmail), getString(etPword), FirebaseInstanceId.getInstance().getToken()).enqueue(new Callback<ObjectHolder<Products>>() {
            @Override
            public void onResponse(Call<ObjectHolder<Products>> call, Response<ObjectHolder<Products>> response) {
                if (response.isSuccessful()){
                    //AccountSharedPrefHelper.getInstance(LoginActivity.this).update(account);
                    startActivity(new Intent(LoginActivity.this, MainActivity.class));
                }else {
                    Toast.makeText(LoginActivity.this, "Failed to Log you in", Toast.LENGTH_SHORT);
                }
                dialog.dismiss();
            }

            @Override
            public void onFailure(Call<ObjectHolder<Products>> call, Throwable t) {
                dialog.dismiss();
                Toast.makeText(LoginActivity.this, "Failed to Log you in", Toast.LENGTH_SHORT);
            }
        });
    }

    private String getString(EditText text){
        return text.getText().toString();
    }
}
