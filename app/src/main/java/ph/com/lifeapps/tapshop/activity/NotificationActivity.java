package ph.com.lifeapps.tapshop.activity;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import ph.com.lifeapps.tapshop.R;
import ph.com.lifeapps.tapshop.adapters.NotificationAdapter;
import ph.com.lifeapps.tapshop.models.Branch;
import ph.com.lifeapps.tapshop.models.Notification;

/**
 * Created by Lem on 7/7/2016.
 */
public class NotificationActivity extends BaseActivity {
    @BindView(R.id.iv_back)
    ImageView ivBackBtn;
    @BindView(R.id.iv_notif_icon)
    ImageView ivNotifBtn;
    @BindView(R.id.rv_notifs)
    RecyclerView rvNotifs;

    @Override
    protected int getLayout() {
        return R.layout.activity_notifications;
    }

    @Override
    protected void initViews() {
        ivNotifBtn.setVisibility(View.GONE);
        initRecyclerView();
    }

    private void initRecyclerView(){
        rvNotifs.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvNotifs.setAdapter(new NotificationAdapter(getNotifications()));
    }

    private List<Notification> getNotifications(){
        List<Notification> notifications = new ArrayList<>();

        Notification notification = new Notification();
        notification.setType(Notification.TYPE.ALMOST);
        notification.setBranch(getBranch());
        notifications.add(notification);

        Notification notification2 = new Notification();
        notification2.setType(Notification.TYPE.INFRONT);
        notification2.setBranch(getBranch());
        notifications.add(notification2);

        Notification notification3 = new Notification();
        notification3.setType(Notification.TYPE.EXPIRED);
        notification3.setBranch(getBranch());
        notifications.add(notification3);

        return notifications;
    }

    private Branch getBranch(){
        Branch branch = new Branch();
        branch.setId(1);
        branch.setName("Uniqlo Megamall");
        return branch;
    }
}
