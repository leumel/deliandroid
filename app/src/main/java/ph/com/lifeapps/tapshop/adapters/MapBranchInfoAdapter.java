package ph.com.lifeapps.tapshop.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import ph.com.lifeapps.tapshop.R;

/**
 * Created by Lem on 6/25/2016.
 */
public class MapBranchInfoAdapter implements GoogleMap.InfoWindowAdapter {

    private Context context;

    public MapBranchInfoAdapter(Context context){
        this.context = context;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return LayoutInflater.from(context).inflate(R.layout.item_branch_info, null);
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }
}
