package ph.com.lifeapps.tapshop.fragment;

import android.content.Intent;

import butterknife.OnClick;
import ph.com.lifeapps.tapshop.R;
import ph.com.lifeapps.tapshop.activity.LoginActivity;
import ph.com.lifeapps.tapshop.models.AccountSharedPrefHelper;

/**
 * Created by Lem on 7/13/2016.
 */
public class ProfileFragment extends BaseFragment {
    @Override
    protected void initViews() {

    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_profile;
    }

    @OnClick(R.id.btn_logout)
    public void logout(){
        AccountSharedPrefHelper.getInstance(getBaseActivity()).clear();
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
