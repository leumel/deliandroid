package ph.com.lifeapps.tapshop.utils;

import android.app.Application;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import java.util.Observable;

/**
 * Created by lemuel.castro on 5/10/2016.
 * <p/>
 * <p/>
 * Main location provider for all Location related views
 * Pull latest location here instead of each view having its own location pool
 * <p/>
 * If Location changes (Due to Location Filter) update Target Location here
 */
public class LocationUpdateSingleton extends Observable implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private static LocationUpdateSingleton locationUpdateSingleton;

    private final Application application;

    private GoogleApiClient mGoogleApiClient;
    private OnLocationDetectedListener onLocationDetectedListener;

    private LatLng detectedLatLng;

    public interface OnLocationDetectedListener {
        void onLocationDetected(LatLng latLng);
    }

    private LocationUpdateSingleton(Application application) {
        this.application = application;
        initGoogleAPIClient();
        mGoogleApiClient.connect();
    }

    public static LocationUpdateSingleton getInstance(Application application, OnLocationDetectedListener onLocationDetectedListener) {
        if (locationUpdateSingleton == null) {
            locationUpdateSingleton = new LocationUpdateSingleton(application);
        }
        locationUpdateSingleton.setOnLocationDetectedListener(onLocationDetectedListener);
        return locationUpdateSingleton;
    }

    private void initGoogleAPIClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(application)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }
    }

    private void startLocationUpdate() {
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, getLocationRequest(), new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.d("lem", "loc detected");
                detectedLatLng = new LatLng(location.getLatitude(), location.getLongitude());
                if (onLocationDetectedListener != null) {
                    onLocationDetectedListener.onLocationDetected(detectedLatLng);
                }
            }
        });
    }

    private LocationRequest getLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setSmallestDisplacement(500);
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    @Override
    public void onConnected(Bundle bundle) {
        Log.d("lem", "connected");
        Location currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        startLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    public void setOnLocationDetectedListener(OnLocationDetectedListener onLocationDetectedListener) {
        this.onLocationDetectedListener = onLocationDetectedListener;
    }

    public OnLocationDetectedListener getOnLocationDetectedListener() {
        return onLocationDetectedListener;
    }

    public LatLng getDetectedLatLng() {
        return detectedLatLng;
    }

    public void setDetectedLatLng(LatLng detectedLatLng) {
        this.detectedLatLng = detectedLatLng;
    }
}

