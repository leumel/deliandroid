package ph.com.lifeapps.tapshop.fragment;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import ph.com.lifeapps.tapshop.R;
import ph.com.lifeapps.tapshop.activity.ShopActivity;
import ph.com.lifeapps.tapshop.adapters.MapBranchInfoAdapter;
import ph.com.lifeapps.tapshop.models.Branch;
import ph.com.lifeapps.tapshop.models.Branches;
import ph.com.lifeapps.tapshop.models.ObjectHolder;
import ph.com.lifeapps.tapshop.network.TapClient;
import ph.com.lifeapps.tapshop.utils.TapUtils;
import ph.com.lifeapps.tapshop.utils.LocationUpdateSingleton;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lem on 7/7/2016.
 */
public class MapFragment extends BaseFragment implements LocationUpdateSingleton.OnLocationDetectedListener, OnMapReadyCallback {

    @BindView(R.id.ll_branch_info)
    LinearLayout llBranchInfo;
    @BindView(R.id.tv_branch_name)
    TextView tvBranchName;
    @BindView(R.id.tv_branch_address)
    TextView tvBranchAddress;
    @BindView(R.id.tv_distance)
    TextView tvDistance;
    @BindView(R.id.fl_map_holder)
    FrameLayout flMapHolder;

    private LocationUpdateSingleton locationUpdateSingleton;
    private static final float ZOOM = 13.0f;
    private static final int MAP_ANIM_DURATION = 500;


    private SupportMapFragment supportMapFragment;
    private GoogleMap googleMap;

    private String currentMap;

    private float originalY;
    private int branchInfoViewHeight;

    private boolean isShown = true;

    private List<Branch> branches;
    private int currentIndex;

    @Override
    protected void initViews() {
        locationUpdateSingleton = LocationUpdateSingleton.getInstance(getActivity().getApplication(), this);

        supportMapFragment = SupportMapFragment.newInstance();
        getChildFragmentManager().beginTransaction().replace(R.id.fl_map_holder, supportMapFragment).commit();
        supportMapFragment.getMapAsync(this);

        llBranchInfo.getViewTreeObserver().addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            @Override
            public boolean onPreDraw() {
                branchInfoViewHeight = llBranchInfo.getHeight();
                originalY = llBranchInfo.getY();
                Log.d("lem", "height "+branchInfoViewHeight);
                llBranchInfo.setY(llBranchInfo.getY()+branchInfoViewHeight+ TapUtils.dpToPx(getContext(), 70));
                llBranchInfo.getViewTreeObserver().removeOnPreDrawListener(this);
                return false;
            }
        });

    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_map;
    }

    @Override
    public void onLocationDetected(LatLng latLng) {
        Log.d("lem", "loc detected");
        if (googleMap!=null){
            moveCamera(new LatLng(14.5764802, 121.0342114));
            getNearestBranches(new LatLng(14.5764802, 121.0342114));
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                moveCamera(new LatLng(marker.getPosition().latitude, marker.getPosition().longitude));
                currentIndex = Integer.parseInt(marker.getTitle());
                tvBranchName.setText(branches.get(currentIndex).getName());
                tvBranchAddress.setText(branches.get(currentIndex).getAddress());
                tvDistance.setText(String.format("%.2f",branches.get(currentIndex).getDistance())+" km");
                animateBranchInfo(llBranchInfo, null);
                return true;
            }
        });
        if (locationUpdateSingleton.getDetectedLatLng()!=null){
            moveCamera(new LatLng(14.5764802, 121.0342114));
            getNearestBranches(new LatLng(14.5764802, 121.0342114));
        }
    }

    @OnClick(R.id.ic_shop_btn)
    public final void startShop(){
        Intent intent = new Intent(getActivity(), ShopActivity.class);
        intent.putExtra(ShopActivity.SHOP, new Gson().toJson(branches.get(currentIndex)));
        getBaseActivity().startActivity(intent);
    }

    private void moveCamera(LatLng latLng){
        CameraPosition cameraPosition = new CameraPosition(latLng, ZOOM, 0.0f, 0.0f);
        CameraUpdate camFactory = CameraUpdateFactory.newCameraPosition(cameraPosition);
        googleMap.animateCamera(camFactory, MAP_ANIM_DURATION, null);
    }

    private void addMarker(){
        googleMap.addMarker(new MarkerOptions().position(locationUpdateSingleton.getDetectedLatLng()).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_activated)));
        googleMap.setInfoWindowAdapter(new MapBranchInfoAdapter(getActivity()));
    }

    private void getNearestBranches(LatLng latLng){
        TapClient.getInstance().getService().getNearestBranch(Double.toString(latLng.latitude), Double.toString(latLng.longitude), TapUtils.getDeviceToken()).enqueue(new Callback<ObjectHolder<Branches>>() {
            @Override
            public void onResponse(Call<ObjectHolder<Branches>> call, Response<ObjectHolder<Branches>> response) {
                int i = 0;
                branches = response.body().getData().getBranches();
                for (Branch branch : branches){
                    googleMap.addMarker(new MarkerOptions().title(Integer.toString(i++)).position(new LatLng(branch.getLatitude(), branch.getLongitude())));
                }
            }
            @Override
            public void onFailure(Call<ObjectHolder<Branches>> call, Throwable t) {

            }
        });
    }

    private void animateBranchInfo(View view, Runnable runnable){
        float from = isShown ? llBranchInfo.getY() : originalY;
        float to = isShown ? originalY : originalY  +branchInfoViewHeight+ TapUtils.dpToPx(getActivity(), 70);

        ObjectAnimator anim = ObjectAnimator.ofFloat(view, "Y", from, to);
        anim.setDuration(MAP_ANIM_DURATION);
        anim.start();

        isShown = !isShown;
    }
}
