package ph.com.lifeapps.tapshop.models;

/**
 * Created by Lem on 7/11/2016.
 */
public class AccountHolder {
    private Account customer;

    public Account getCustomer() {
        return customer;
    }

    public void setCustomer(Account customer) {
        this.customer = customer;
    }
}
