package ph.com.lifeapps.tapshop.models;

/**
 * Created by Lem on 6/21/2016.
 */
public class Branch {
    private int id;
    private String address;
    private String company_id;
    private String name;
    private double latitude;
    private double longitude;
    private String has_delivery;
    private String has_pickup;
    private double distance;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getHas_delivery() {
        return has_delivery;
    }

    public void setHas_delivery(String has_delivery) {
        this.has_delivery = has_delivery;
    }

    public String getHas_pickup() {
        return has_pickup;
    }

    public void setHas_pickup(String has_pickup) {
        this.has_pickup = has_pickup;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
