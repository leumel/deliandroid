package ph.com.lifeapps.tapshop.network;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import ph.com.lifeapps.tapshop.BuildConfig;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Lem on 6/15/2016.
 */
public class TapClient {

    private static TapClient tapClient;
    private final TapService service;

    private TapClient(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.ENDPOINT)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        service = retrofit.create(TapService.class);
    }

    private static final void init(){
        tapClient = new TapClient();
    }

    public static final TapClient getInstance(){
        if (tapClient == null){
            init();
        }
        return tapClient;
    }

    public TapService getService() {
        return service;
    }
}
