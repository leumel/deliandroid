package ph.com.lifeapps.tapshop.models;

import java.util.List;

/**
 * Created by Lem on 7/6/2016.
 */
public class Products {
    private List<Product> products;

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
}
