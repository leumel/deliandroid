package ph.com.lifeapps.tapshop.utils;

import android.content.Context;
import android.util.DisplayMetrics;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by Lem on 6/25/2016.
 */
public class TapUtils {

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    public static int pxToDp(Context context, int px) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int dp = Math.round(px / (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

    public static String getDeviceToken(){
        return FirebaseInstanceId.getInstance().getToken();
    }
}
