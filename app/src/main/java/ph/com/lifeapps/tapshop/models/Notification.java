package ph.com.lifeapps.tapshop.models;

import ph.com.lifeapps.tapshop.utils.StringUtils;

/**
 * Created by Lem on 7/7/2016.
 */
public class Notification {
    public enum TYPE{
        EXPIRED("You failed to go back on queue. Your slot and order on <b>%s</b> has been revoked."),
        ALMOST("You're almost in front of the queue at <b>%s</b>. We advice you to rush those steps!"),
        INFRONT("You're already in front of the queue at <b>%s</b>. Please proceed to claim area."),
        GENERAL("");

        public final String message;

        TYPE(String message){
            this.message = message;
        }
    }

    private TYPE type;
    private Branch branch;
    private String message;

    public String getMessage(){
        if (branch==null){
            return message;
        }
        return String.format(this.type.message, branch.getName());
    }

    public TYPE getType() {
        return type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
