package ph.com.lifeapps.tapshop.network;

import java.util.HashMap;
import java.util.List;

import okhttp3.RequestBody;
import ph.com.lifeapps.tapshop.models.Branches;
import ph.com.lifeapps.tapshop.models.AccountHolder;
import ph.com.lifeapps.tapshop.models.ObjectHolder;
import ph.com.lifeapps.tapshop.models.Products;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by Lem on 6/15/2016.
 */
public interface TapService {

    @FormUrlEncoded
    @POST("branch/nearest")
    Call<ObjectHolder<Branches>> getNearestBranch(@Field("latitude") String lat, @Field("longitude") String lng, @Field("device_id") String devID);

    @FormUrlEncoded
    @POST("product/search/tag")
    Call<ObjectHolder<Products>> getProductByTag(@Field("branch_id") String branchID, @Field("tag_id") String tadID, @Field("device_id") String devID);

    @FormUrlEncoded
    @POST("order/checkout")
    Call<ObjectHolder<Products>> checkout(@Field("branch_id") String branchID, @Field("expected_amount") String expectedAmnt,
                                          @Field("customerID") String customerID, @Field(value = "order", encoded = false) String order, @Field("device_id") String devID);

    @Multipart
    @POST("customer/register")
    Call<ObjectHolder<AccountHolder>> register(@Part("name") RequestBody name, @Part("email_address") RequestBody emailAdd,
                                               @Part("password") RequestBody pWord, @Part("contact_number") RequestBody contactNum, @Part("image\"; filename=\"pp.png\" ")RequestBody image);

    @FormUrlEncoded
    @POST("customer/login")
    Call<ObjectHolder<Products>> login(@Field("email_address") String emailAdd, @Field("password") String pWord, @Field("device_id") String devID);
}