package ph.com.lifeapps.tapshop.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import ph.com.lifeapps.tapshop.activity.BaseActivity;

/**
 * Created by Lem on 7/7/2016.
 */
public abstract class BaseFragment extends Fragment {

    protected View mainView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, mainView);
        initViews();
        return mainView;
    }

    protected BaseActivity getBaseActivity(){
        return (BaseActivity)getActivity();
    }

    protected abstract void initViews();
    protected abstract int getLayout();
}
