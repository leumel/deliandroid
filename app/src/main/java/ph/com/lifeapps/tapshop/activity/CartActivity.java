package ph.com.lifeapps.tapshop.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import ph.com.lifeapps.tapshop.R;
import ph.com.lifeapps.tapshop.adapters.CartAdapter;
import ph.com.lifeapps.tapshop.interfaces.OnItemClick;
import ph.com.lifeapps.tapshop.models.Account;
import ph.com.lifeapps.tapshop.models.AccountSharedPrefHelper;
import ph.com.lifeapps.tapshop.models.Branch;
import ph.com.lifeapps.tapshop.models.ObjectHolder;
import ph.com.lifeapps.tapshop.models.Product;
import ph.com.lifeapps.tapshop.models.Products;
import ph.com.lifeapps.tapshop.network.TapClient;
import ph.com.lifeapps.tapshop.utils.StringUtils;
import ph.com.lifeapps.tapshop.utils.TapUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Lem on 6/23/2016.
 */
public class CartActivity extends BaseActivity implements OnItemClick {

    public static final String CART_EXTRA = "cart_extra";
    public static final String BRANCH_EXTRA = "branch_extra";

    @BindView(R.id.rv_cart)
    RecyclerView rvCart;
    @BindView(R.id.tv_branch_name)
    TextView tvBranchName;
    @BindView(R.id.tv_label_branch_name)
    TextView tvLabelBranchName;

    @BindView(R.id.tv_total_price)
    TextView tvTotalPrice;
    @BindView(R.id.tv_item_count)
    TextView tvItemCount;

    @BindView(R.id.iv_no_item_indicator)
    ImageView ivNoItemIndicator;

    private HashMap<Product, Integer> items;
    private Branch branch;
    private Account account;
    private ProgressDialog progressDialog;

    @Override
    protected int getLayout() {
        return R.layout.activity_cart;
    }

    @Override
    protected void initViews() {
        account = AccountSharedPrefHelper.getInstance(this).getACCOUNT();
        items = (HashMap<Product, Integer>) getIntent().getSerializableExtra(CART_EXTRA);
        if (items.size() == 0){
            rvCart.setVisibility(View.GONE);
            ivNoItemIndicator.setVisibility(View.VISIBLE);
        }else {
            rvCart.setVisibility(View.VISIBLE);
            ivNoItemIndicator.setVisibility(View.GONE);
        }

        branch = new Gson().fromJson(getIntent().getStringExtra(BRANCH_EXTRA), Branch.class);
        tvLabelBranchName.setText(branch.getName());
        tvTotalPrice.setText(StringUtils.pricify(computeTotalPrice()));
        tvItemCount.setText(Integer.toString(countTotalItems())+" items");
        rvCart.setLayoutManager(new LinearLayoutManager(this));
        initItemsOnCart();
    }

    private double computeTotalPrice() {
        double totalPrice = 0.00f;
        for (Product product : items.keySet()) {
            totalPrice += product.getPrice() * items.get(product);
        }
        return totalPrice;
    }

    private int countTotalItems() {
        int totalItems = 0;
        for (Product product : items.keySet()) {
            totalItems += items.get(product);
        }
        return totalItems;
    }

    private List<Product> getAllItemsOnCart() {
        List<Product> products = new ArrayList();
        Log.d("lem", "getting items");
        for (Product product : items.keySet()) {
            product.setCount(items.get(product));
            Log.d("lem", "product " + product.getName());
            products.add(product);
        }
        Log.d("lem", "done getting items");
        return products;
    }

    @OnClick(R.id.tv_checkout)
    public void checkout(){
        progressDialog = ProgressDialog.show(this, "Loading...", "Checking out items...", false);
        TapClient.getInstance().getService().checkout(Integer.toString(branch.getId()), Double.toString(computeTotalPrice()),
                                                        Integer.toString(account.getId()), createOrderParam(), TapUtils.getDeviceToken()).enqueue(new Callback<ObjectHolder<Products>>() {
            @Override
            public void onResponse(Call<ObjectHolder<Products>> call, Response<ObjectHolder<Products>> response) {
                progressDialog.dismiss();
                if(response.isSuccessful()){
                    Intent intent = new Intent(CartActivity.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<ObjectHolder<Products>> call, Throwable t) {

            }
        });
        createOrderParam();
    }


   private String createOrderParam(){
       StringBuilder builder = new StringBuilder();
       builder.append("[");

       boolean isMoreThanOne = false;
       for (Product product : getAllItemsOnCart()){
           if(isMoreThanOne){
               builder.append(",");
           }
           builder.append("{");
           builder.append("\"id\":"+"\""+Integer.toString(product.getId())+"\"");
           builder.append(",");
           builder.append("\"qty\":"+"\""+Integer.toString(product.getCount())+"\"");
           builder.append(",");
           builder.append("\"comment\":\"\"");
           builder.append("}");
           isMoreThanOne = true;
       }
       builder.append("]");


       return builder.toString();
    }



    private void initItemsOnCart() {
        CartAdapter cartAdapter = new CartAdapter(getAllItemsOnCart(), this);
        rvCart.setAdapter(cartAdapter);
    }

    @Override
    public void onItemClick(int position, Object object) {

    }
}
