package ph.com.lifeapps.tapshop.adapters;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import ph.com.lifeapps.tapshop.R;
import ph.com.lifeapps.tapshop.interfaces.OnItemClick;
import ph.com.lifeapps.tapshop.models.Product;
import ph.com.lifeapps.tapshop.utils.StringUtils;

/**
 * Created by Lem on 6/25/2016.
 */
public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CartViewHolder> {

    private final String IMAGE_ENDPOINT = "http://ec2-54-254-151-183.ap-southeast-1.compute.amazonaws.com/images/products/";

    private static final long DURATION = 300;
    private List<Product> items;
    private Context context;
    private OnItemClick onItemClick;

    public CartAdapter(List<Product> items, OnItemClick onItemClick){
        this.items = items;
        this.onItemClick = onItemClick;
    }

    @Override
    public CartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (context == null){
            context = parent.getContext();
        }
        return new CartViewHolder(LayoutInflater.from(context).inflate(R.layout.item_cart, parent, false));
    }

    @Override
    public void onBindViewHolder(final CartViewHolder holder, int position) {
        Product product = items.get(position);

        Glide.with(context).load(IMAGE_ENDPOINT+product.getImage_url()).into(holder.ivPproductImage);
        holder.tvItemName.setText(product.getName());
        holder.tvItemPerDetail.setText(StringUtils.getPriceInfo(product));
        holder.tvItemTotalPrice.setText(StringUtils.pricify(product));
        holder.tvProductCount.setText(Integer.toString(product.getCount()));

        holder.tvProductCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.llProductDescHolder.getVisibility() == View.VISIBLE){
                    animate(holder.llProductDescHolder, 0, -holder.llProductDescHolder.getWidth(), new Runnable() {
                        @Override
                        public void run() {
                            holder.llProductDescHolder.setVisibility(View.INVISIBLE);
                        }
                    });
                    animate(holder.llProductEdit, holder.llProductEdit.getWidth(), 0, null);
                    holder.llProductEdit.setVisibility(View.VISIBLE);
                }
            }
        });
        holder.ivPproductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.llProductDescHolder.getVisibility() != View.VISIBLE){
                    animate(holder.llProductDescHolder, -holder.llProductDescHolder.getWidth(), 0, null);
                    animate(holder.llProductEdit, 0, holder.llProductEdit.getWidth(), new Runnable() {
                        @Override
                        public void run() {
                            holder.llProductEdit.setVisibility(View.INVISIBLE);
                        }
                    });
                    holder.llProductDescHolder.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void animate(View view, float from, float to, final Runnable endAction){
        ObjectAnimator slide = ObjectAnimator.ofFloat(view,"X", from, to);
        slide.setDuration(DURATION);
        slide.start();
        slide.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                if (endAction != null){
                    new Handler().post(endAction);
                }
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return items.size();
    }

    public class CartViewHolder extends RecyclerView.ViewHolder{


        @BindView(R.id.ll_product_desc_holder)      View llProductDescHolder;
        @BindView(R.id.ll_product_edit_order)       View llProductEdit;
        @BindView(R.id.iv_prod_photo)               ImageView ivPproductImage;
        @BindView(R.id.tv_prod_count)               TextView tvProductCount;
        @BindView(R.id.tv_item_name)                TextView tvItemName;
        @BindView(R.id.tv_per_detail)               TextView tvItemPerDetail;
        @BindView(R.id.tv_item_total_price)         TextView tvItemTotalPrice;

        public CartViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
