package ph.com.lifeapps.tapshop.models;

import java.util.List;

/**
 * Created by Lem on 6/21/2016.
 */
public class Branches {
    private List<Branch> branches;

    public List<Branch> getBranches() {
        return branches;
    }

    public void setBranches(List<Branch> branches) {
        this.branches = branches;
    }
}
