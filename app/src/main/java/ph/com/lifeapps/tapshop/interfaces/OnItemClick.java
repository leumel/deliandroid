package ph.com.lifeapps.tapshop.interfaces;

/**
 * Created by Lem on 6/25/2016.
 */
public interface OnItemClick<K> {
    void onItemClick(int position, K object);
}
