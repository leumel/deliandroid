package ph.com.lifeapps.tapshop.fragment;

import ph.com.lifeapps.tapshop.R;

/**
 * Created by Lem on 7/7/2016.
 */
public class ComingSoonFragment extends BaseFragment {
    @Override
    protected void initViews() {

    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_coming_soon;
    }
}
