package ph.com.lifeapps.tapshop.services;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by Lem on 7/7/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        Log.d("lem", "From: " + remoteMessage.getFrom());
        Log.d("lem", "Notification Message Body: " + remoteMessage.getNotification().getBody());
    }
}
